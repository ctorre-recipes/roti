Procedure
=========

1. Put all ingredients together into a bowl and mix to form a dough.
2. Heat pan over hot heat.
3. Take dough and form into small balls and roll out into squares.
4. Salt pan and then add dough, cook for about 1-2 minutes each side or until slightly charred.
5. Once cooked, slice into 4 squares per person.
